const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const autoprefixer = require('autoprefixer');
const fs = require('fs');

const devServerPort = 5000;
const assetsFolderName = 'assets';

function generateHtmlPlugins(templateDir) {
    const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
    return templateFiles.map(item => {
        const parts = item.split('.');
        const name = parts[0];
        const extension = parts[1];
        return new HtmlWebpackPlugin({
            filename: `${name}.html`,
            template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
            inject: false,
        })
    })
}

const htmlPlugins = generateHtmlPlugins('./src/html/views');

module.exports = {
    entry: [
        // './src/scss/style.scss',
        './src/js/index.js',
    ],
    output: {
        filename: './js/bundle.js'
    },
    devServer :{
        contentBase: path.join(__dirname, assetsFolderName),
        compress: true,
        port: devServerPort,
        overlay: {
            warnings: true,
            errors: true
        },
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    },

    devtool: "source-map",

    module: {
        rules: [
            // js
            {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src/js'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', {
                                modules: false
                            }],
                        ],
                        plugins: ['@babel/plugin-proposal-class-properties'],
                    }
                }
            },
            // scss | sass            
            {
                test: /\.(sass|scss)$/,
                include: path.resolve(__dirname, 'src/scss'),
                use: [{
                        loader: MiniCssExtractPlugin.loader,
                        options: {}
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                            url: false
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            sourceMap: true,
                            plugins: () => [
                                require('cssnano')({
                                    preset: ['default', {
                                        discardComments: {
                                            removeAll: true,
                                        },
                                    }]
                                }),
                                autoprefixer({
                                    browsers: ['ie >= 8', 'Firefox >= 14', 'last 4 version']
                                })
                            ]
                        }
                    },                    
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        }
                    }                    
                ]
            },

            // Images
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',{
                        loader: 'image-wbpack-loade',
                        options:{
                            mozjpeg:{
                                progressive: true,
                                quality: 70
                            }
                        }
                    }
                ]
            },

            // html
            {
                test: /\.html$/,
                include: path.resolve(__dirname, 'src/html/includes'),
                use: ['raw-loader']
            },
        ]
    },
    plugins: [
        new CleanWebpackPlugin('dist', {}),
        new MiniCssExtractPlugin({
            filename: "./css/style.bundle.css"
        }),
        new CopyWebpackPlugin([{
                from: './src/font',
                to: './font'
            },
            {
                from: './src/favicon',
                to: './favicon'
            },
            {
                from: './src/img',
                to: './img'
            },
            {
                from: './src/uploads',
                to: './uploads'
            },
            {
                from: './src/js/export_js',
                to: './js'
            }
        ]),
    ].concat(htmlPlugins)
};