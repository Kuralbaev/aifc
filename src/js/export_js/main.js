
$(document).ready(function () {
    $(".icon").click(function () {
        $(this).toggleClass("open");
        $("#sidebar").toggleClass("active");
        $("body").toggleClass("hidden")
    })
});

$(function () {
    $('.min-chart#chart-sales').easyPieChart({
        barColor: "#2ad474",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});

$(function () {
    $('.min-chart#chart-sales_red').easyPieChart({
        barColor: "#fe5656",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});

$(function () {
    $('.min-chart#chart-sales_yellow').easyPieChart({
        barColor: "#fed656",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});

// Data Picker Initialization
$('.datepicker').pickadate({
    // Strings and translations
    monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь	', 'Ноябрь', 'Декабрь'],
    monthsShort: ['Янв', 'Феб', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    weekdaysFull: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
    weekdaysShort: ['Пон', 'Втр', 'Срд', 'Чет', 'Пят', 'Суб', 'Вос'],
    showMonthsShort: undefined,
    showWeekdaysFull: undefined,


    // Buttons
    today: 'Сегодня',
    clear: 'Очистить',
    close: 'Закрыть',

    // Accessibility labels
    labelMonthNext: 'Next month',
    labelMonthPrev: 'Previous month',
    labelMonthSelect: 'Select a month',
    labelYearSelect: 'Select a year',

    // Formats
    format: 'd mmmm, yyyy',
    formatSubmit: undefined,
    hiddenPrefix: undefined,
    hiddenSuffix: '_submit',
    hiddenName: undefined,

    // Editable input
    editable: undefined,

    // Dropdown selectors
    selectYears: undefined,
    selectMonths: undefined,

    // First day of the week
    firstDay: undefined,

    // Date limits
    min: undefined,
    max: undefined,

    // Disable dates
    disable: undefined,

    // Root picker container
    container: undefined,

    // Hidden input container
    containerHidden: undefined,

    // Close on a user action
    closeOnSelect: true,
    closeOnClear: true,

    // Events
    onStart: undefined,
    onRender: undefined,
    onOpen: undefined,
    onClose: undefined,
    onSet: undefined,
    onStop: undefined,

    // Classes
    klass: {

        // The element states
        input: 'picker__input',
        active: 'picker__input--active',

        // The root picker and states *
        picker: 'picker',
        opened: 'picker--opened',
        focused: 'picker--focused',

        // The picker holder
        holder: 'picker__holder',

        // The picker frame, wrapper, and box
        frame: 'picker__frame',
        wrap: 'picker__wrap',
        box: 'picker__box',

        // The picker header
        header: 'picker__header',

        // Month navigation
        navPrev: 'picker__nav--prev',
        navNext: 'picker__nav--next',
        navDisabled: 'picker__nav--disabled',

        // Month & year labels
        month: 'picker__month',
        year: 'picker__year',

        // Month & year dropdowns
        selectMonth: 'picker__select--month',
        selectYear: 'picker__select--year',

        // Table of dates
        table: 'picker__table',

        // Weekday labels
        weekdays: 'picker__weekday',

        // Day states
        day: 'picker__day',
        disabled: 'picker__day--disabled',
        selected: 'picker__day--selected',
        highlighted: 'picker__day--highlighted',
        now: 'picker__day--today',
        infocus: 'picker__day--infocus',
        outfocus: 'picker__day--outfocus',

        // The picker footer
        footer: 'picker__footer',

        // Today, clear, & close buttons
        buttonClear: 'picker__button--clear',
        buttonClose: 'picker__button--close',
        buttonToday: 'picker__button--today'
    }
});


$('#input_starttime').pickatime({
    // 12 or 24 hour
    twelvehour: false,
});
$('#input_starttime_1').pickatime({
    // 12 or 24 hour
    twelvehour: false,
});
$(document).ready(function () {
    $('.mdb-select').material_select();
});


// Mask

$(function () {
    $("#time_1").mask("99 : 99");
    $("#time_2").mask("99 : 99");
});



// Diogramm

    $(document).ready(function () {
        $('#dtBasicExample').DataTable({
            // paging: false,
            "language": {
                "paginate": {
                    "previous": "Назад"
                }
            },
            searching: false,
            info: false
        });
        $('.dataTables_length').addClass('bs-select');
    });

//exporte les données sélectionnées
var $table = $('#table');
$(function () {
    $('#toolbar').find('select').change(function () {
        $table.bootstrapTable('refreshOptions', {
            exportDataType: $(this).val()
        });
    });
})

var trBoldBlue = $("table");

$(trBoldBlue).on("click", "tr", function () {
    $(this).toggleClass("bold-blue");
});

//Diogram Blue

var bar_ctx = document.getElementById('chart').getContext('2d');

var purple_orange_gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(bar_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: '',
            data: [12, 19, 3, 8, 14, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});



var b_ctx = document.getElementById('chart_1').getContext('2d');

var purple_orange_gradient = b_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(b_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [1, 15, 80, 20, 50, 95],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var b_ctx = document.getElementById('chart_2').getContext('2d');

var purple_orange_gradient = b_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(b_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [1, 55, 80, 65, 10, 95],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});

var b_ctx = document.getElementById('chart_3').getContext('2d');

var purple_orange_gradient = b_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(b_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [63, 15, 80, 55, 50, 95],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var b_ctx = document.getElementById('chart_4').getContext('2d');

var purple_orange_gradient = b_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(b_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [6, 10, 50, 6, 60, 95],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});



// Diogram Yellow

var yellow_ctx = document.getElementById('chart_yellow').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [1, 15, 10, 20, 25, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var yellow_ctx = document.getElementById('chart_6').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [1, 44, 19, 40, 25, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var yellow_ctx = document.getElementById('chart_10').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [10, 15, 90, 20, 25, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var yellow_ctx = document.getElementById('chart_11').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [90, 15, 90, 20, 25, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var yellow_ctx = document.getElementById('chart_12').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [50, 15, 10, 20, 25, 59],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});

// var yellow_ctx = document.getElementById('chart_7').getContext('2d');
//
// var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
// purple_orange_gradient.addColorStop(0, '#f5d100 ');
// purple_orange_gradient.addColorStop(1, '#5ecd74');


// Diogram Cercle

$(function () {
    $('.min-chart#chart-sales').easyPieChart({
        barColor: "#2ad474",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});


